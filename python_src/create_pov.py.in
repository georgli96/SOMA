#!/usr/bin/env python

#   Copyright (C) 2016-2019 Ludwig Schneider
#   Copyright (C) 2016 N. Harshavardhan Reddy
#
# This file is part of SOMA.
#
# SOMA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SOMA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with SOMA.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.append( @SOMA_PYTHON_INSTALL_DIR@ )
import h5py
import argparse
import numpy as np
import math
import ConfGen
import random

def get_header(arguments):
	string = ""
	string += "#include \"colors.inc\"\n"
	bg_color="White"
	if arguments['bg']:
		bg_color = arguments['bg']
	string += "background{"+bg_color+"}\n"
	return string

def get_camera(arguments, lxyz):
	string = ""
	camera_angle = 15
	if arguments['ca']:
		camera_angle = arguments['ca']
		if camera_angle <= 0:
			print("Invalid camera angle. Setting the camera angle to 15 degrees")
			camera_angle = 15
	camera_look_at = "<"+str(lxyz[0]/2)+","+str(lxyz[1]/2)+","+str(lxyz[2]/2)+">"
	if arguments['cla']:
		camera_look_at=arguments['cla']
	camera_location = "<"+str(lxyz[0]/2)+","+str(lxyz[1]/2)+","+str(10+lxyz[2]+(lxyz[0]/(2*math.tan(15*3.14/360))))+">" #tries to include the entire image
	if arguments['cl']:
		camera_location = arguments['cl']

	print("\nCamera angle: "+str(camera_angle))
	print("Camera location: "+camera_location)
	print("Camera look at: "+camera_look_at)

	string += "camera { angle "+str(camera_angle)+" location "+camera_location+" look_at "+camera_look_at+" }\n"
	return string

def get_light(arguments, lxyz):
	string=""
	light_location = "<4000,2000, 2000>"
	if arguments['l']:
		light_location = arguments['l']
	print("Light location: :"+light_location)
	string += "light_source{ "+light_location+" color White} \n"
	return string

def cycle(x, len): #for periodicity
	if x < 0:
		x = x + len
		x = cycle(x, len)
	if x > len:
		x = x - len
		x = cycle(x, len)
	return x

#to make sure cylinders dont go through the borders
def bordercheck(x0, x1, len):
	if x1-x0 > len/2: x1 = 0
	if x1-x0 < -len/2: x1 = len
	return x1

def get_dimensions(arguments):
	f = h5py.File(arguments['i'],"r")
	if "/parameter/lxyz" in f:
		lxyz = f["/parameter/lxyz"][()]
	f.close()
	return lxyz

def get_bead_color(arguments, poly_type_offset,p_type, n_types, poly_arch):
	poly_length = poly_arch[poly_type_offset[p_type]]
	bead_color = np.chararray((poly_length+1), itemsize=20)
	bead_visible = np.ones((poly_length))
	bead_type = np.zeros((poly_length))
	type_color = np.chararray((n_types), itemsize=20)
	bead_color[:] = " "
	type_color[:] = " "

	for bead in range(poly_length):
		bead_type[bead] = ConfGen.get_particle_type(poly_arch[poly_type_offset[p_type]+bead+1])

	if arguments['t']:
		string = arguments['t']
		type_string = string.split("/")
		for str_num in range(len(type_string)):
			type_color[int(type_string[str_num].split(":")[0])] = type_string[str_num].split(":")[1]
		for bead in range(poly_length):
			#print(bead)
			#print(type_color[bead_type[bead]])
			bead_color[bead] = type_color[bead_type[bead]]
			if bead_color[bead]!=bead_color[poly_length]:
                                bead_visible[bead] = 0
	return bead_color, bead_visible, type_color

def get_percentage(arguments, n_poly_type):
	percentage = np.zeros(n_poly_type)
	percentage[:]=20
	if arguments['p']:
		percentage[:] = 0
		string = arguments['p']
		type_string = string.split("/")
		for str_num in range(len(type_string)):
			percentage[int(type_string[str_num].split(":")[0])] = int(type_string[str_num].split(":")[1])
			if int(type_string[str_num].split(":")[1]) > 100 or int(type_string[str_num].split(":")[1]) < 0:
				percentage[int(type_string[str_num].split(":")[0])] = 0
	for poly_type in range(n_poly_type):
		print("Plotting polymer Type"+str(poly_type)+" with "+str(percentage[poly_type])+"%")
	return percentage

def get_visibility(percentage):
	visibility = 0
	i = random.random()*100
	if i <= percentage: visibility =1
	return visibility

def main(argv):
	parser = argparse.ArgumentParser(description="Script to create povray input files.")
    	parser.add_argument('-i',metavar='input_file',type=str,required=True,help="Name of the input configuration file cotaining bead data.")
    	parser.add_argument('-o',metavar='output_file',type=str,required=False,help="Name of the output file. Will be  overwritten.")
    	parser.add_argument('-t',metavar='types',type=str,required=True,help="Which types should be displayed along with the color of beads. All types will be displayed by default. Format: \"[type]:[color]/[type][color]\". The color must be a pov ray color.")
    	parser.add_argument('-p',metavar='poly_percent',type=str,required=False,help="Percentage of polymers to be visible. Default is 20. Input a float between 0 and 100.")
    	parser.add_argument('-cl',metavar='camera_location',type=str,required=False,help="camera location in pov ray format: \"<x, y, z>\"")
    	parser.add_argument('-cla',metavar='camera_look_at',type=str,required=False,help="camera look_at in pov ray format: \"<x, y, z>\"")
    	parser.add_argument('-ca',metavar='camera_angle',type=float,required=False,help="camera angle in degrees as a float > 0")
    	parser.add_argument('-l',metavar='light_location',type=str,required=False,help="light location in pov ray format: \"<x, y, z>\"")
    	parser.add_argument('-bg',metavar='bg_color',type=str,required=False,help="Background color from pov ray colors. Default is White.")

    	arguments = vars(parser.parse_args())

    	if arguments['i']:
    		input_file = arguments['i']
                assert input_file.find(".h5") >= 0, "Error: input file: '"+input_file+"' does not contain '.h5'"

	f = h5py.File(input_file,'r')
	assert 'beads' in f, "Error: input file: '"+input_file+"' does not contain 'beads'"

	shape = f['/beads'].shape #number of polymers, polmer length, 3
	dset = f["/beads"][()] #stores bead data
	lxyz = get_dimensions(arguments) #dimensions of the simulation in terms of Re
	print("Dimensions of the box is "+str(lxyz)+"\n")

	n_types = f["/parameter/n_types"][()][0]
	n_poly_type = f["/parameter/n_poly_type"][()][0]
	poly_arch = f["/parameter/poly_arch"][()]
	poly_type = f["/poly_type"][()]
	poly_type_offset = f["/parameter/poly_type_offset"][()]

	percentage = get_percentage(arguments, n_poly_type)

	bead_color, bead_visible, type_color = get_bead_color(arguments, poly_type_offset, 0, n_types, poly_arch)
	for type in range(n_types):
		if type_color[type]!=" ":print("Plotting Type "+str(type)+" with color "+type_color[type])

	string = ""
	string += get_header(arguments)
	string += get_camera(arguments, lxyz)
	string += get_light(arguments, lxyz)
	string += "\n"

	for polymer in range(shape[0]):
		p_type = poly_type[polymer]
		visibility = get_visibility(percentage[p_type])
		if visibility == 0: continue
		poly_length = poly_arch[poly_type_offset[p_type]]
		bead_color, bead_visible, type_color = get_bead_color(arguments, poly_type_offset, p_type, n_types, poly_arch)

		for bead in range(poly_length):
			flag = 0 #only if flag==0 will the bead be shown. So you can manipulate flag in this program to get the preferred image
			color = bead_color[bead]
			flag = bead_visible[bead] #here the color depends on the type, you can change the color from here. Pov ray colors as a string

			bond_start = ConfGen.get_bondlist_offset(poly_arch[poly_type_offset[p_type]+bead+1])#index to read bond data
                        i = bond_start
                        end = 0
                        while end == 0:
                                info = poly_arch[i]
                                i += 1
			        end = ConfGen.get_end(info)
			        bond_offset = ConfGen.get_offset(info)#offset to neighbor bead

			        #if flag == 0:
                                if flag == 0 and bond_offset > 0:
				        x0 = cycle(dset[polymer][bead][0], lxyz[0])
				        y0 = cycle(dset[polymer][bead][1], lxyz[1])
				        z0 = cycle(dset[polymer][bead][2], lxyz[2])
				        x1 = cycle(dset[polymer][bead+bond_offset][0], lxyz[0])
				        y1 = cycle(dset[polymer][bead+bond_offset][1], lxyz[1])
				        z1 = cycle(dset[polymer][bead+bond_offset][2], lxyz[2])
				        x1 = bordercheck(x0, x1, lxyz[0])
				        y1 = bordercheck(y0, y1, lxyz[1])
				        z1 = bordercheck(z0, z1, lxyz[2])

				        string += "cylinder { <"+str(x0)+","+str(y0)+","+str(z0)+">, <"+str(x1)+","+str(y1)+","+str(z1)+">, 0.01 texture{ pigment{ color "+color+" } } }\n"

	output_file = input_file[:-3]+".pov"
	if arguments['o']:
    		output_file = arguments['o']

	out = open(output_file,"w")
	out.write(string)
	out.close()

if __name__ =="__main__":
    main(sys.argv)
